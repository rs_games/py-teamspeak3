from distutils.core import setup

__version__ = "0.1"
__doc__ = """Python binding for the TeamSpeak 3 SDK"""

setup(
    name = "py_teamspeak3",
    version = __version__,
    description = __doc__,
    py_modules = ['ts3'],
    classifiers = [
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'License :: OSI Approved :: BSD License',
        'Topic :: Software Development :: Libraries',
    ],
)
