import os
import platform
import time
from ctypes import *

class TS3Error (Exception):
 """Error that is raised when there is a problem with a TS3 call."""

def handle_error(result, func, args):
    if result != 0 and result != 3:
        raise TS3Error('TS3Error calling %s, errorCode=%s' % (func.__name__, hex(result)))

if (platform.system() == 'Windows'):
    if (platform.architecture()[0] == '64bit'):
        dll = cdll.LoadLibrary(os.path.join(os.getcwd(), 'ts3client_win64.dll'))
    else:
        dll = cdll.LoadLibrary(os.path.join(os.getcwd(), 'ts3client_win32.dll'))
elif platform.system() == 'Darwin':
    dll = cdll.LoadLibrary(os.path.join(os.getcwd(), 'libts3client_mac.dylib'))
else:
    dll = cdll.LoadLibrary(os.path.join(os.getcwd(), 'libts3client_linux_x86.so'))

_libs = {'ts3': dll}

# Public Definitions
enum_TalkStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 39

STATUS_NOT_TALKING = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 39

STATUS_TALKING = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 39

STATUS_TALKING_WHILE_DISABLED = 2 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 39

enum_CodecType = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 45

CODEC_SPEEX_NARROWBAND = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 45

CODEC_SPEEX_WIDEBAND = (CODEC_SPEEX_NARROWBAND + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 45

CODEC_SPEEX_ULTRAWIDEBAND = (CODEC_SPEEX_WIDEBAND + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 45

CODEC_CELT_MONO = (CODEC_SPEEX_ULTRAWIDEBAND + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 45

enum_CodecEncryptionMode = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 52

CODEC_ENCRYPTION_PER_CHANNEL = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 52

CODEC_ENCRYPTION_FORCED_OFF = (CODEC_ENCRYPTION_PER_CHANNEL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 52

CODEC_ENCRYPTION_FORCED_ON = (CODEC_ENCRYPTION_FORCED_OFF + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 52

enum_TextMessageTargetMode = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 58

TextMessageTarget_CLIENT = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 58

TextMessageTarget_CHANNEL = (TextMessageTarget_CLIENT + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 58

TextMessageTarget_SERVER = (TextMessageTarget_CHANNEL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 58

TextMessageTarget_MAX = (TextMessageTarget_SERVER + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 58

enum_MuteInputStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 65

MUTEINPUT_NONE = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 65

MUTEINPUT_MUTED = (MUTEINPUT_NONE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 65

enum_MuteOutputStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 70

MUTEOUTPUT_NONE = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 70

MUTEOUTPUT_MUTED = (MUTEOUTPUT_NONE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 70

enum_HardwareInputStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 75

HARDWAREINPUT_DISABLED = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 75

HARDWAREINPUT_ENABLED = (HARDWAREINPUT_DISABLED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 75

enum_HardwareOutputStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 80

HARDWAREOUTPUT_DISABLED = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 80

HARDWAREOUTPUT_ENABLED = (HARDWAREOUTPUT_DISABLED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 80

enum_InputDeactivationStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 85

INPUT_ACTIVE = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 85

INPUT_DEACTIVATED = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 85

enum_ReasonIdentifier = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_NONE = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_MOVED = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_SUBSCRIPTION = 2 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_LOST_CONNECTION = 3 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_KICK_CHANNEL = 4 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_KICK_SERVER = 5 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_KICK_SERVER_BAN = 6 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_SERVERSTOP = 7 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_CLIENTDISCONNECT = 8 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_CHANNELUPDATE = 9 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_CHANNELEDIT = 10 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

REASON_CLIENTDISCONNECT_SERVER_SHUTDOWN = 11 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 90

enum_ChannelProperties = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_NAME = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_TOPIC = (CHANNEL_NAME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_DESCRIPTION = (CHANNEL_TOPIC + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_PASSWORD = (CHANNEL_DESCRIPTION + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_CODEC = (CHANNEL_PASSWORD + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_CODEC_QUALITY = (CHANNEL_CODEC + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_MAXCLIENTS = (CHANNEL_CODEC_QUALITY + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_MAXFAMILYCLIENTS = (CHANNEL_MAXCLIENTS + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_ORDER = (CHANNEL_MAXFAMILYCLIENTS + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_FLAG_PERMANENT = (CHANNEL_ORDER + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_FLAG_SEMI_PERMANENT = (CHANNEL_FLAG_PERMANENT + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_FLAG_DEFAULT = (CHANNEL_FLAG_SEMI_PERMANENT + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_FLAG_PASSWORD = (CHANNEL_FLAG_DEFAULT + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_CODEC_LATENCY_FACTOR = (CHANNEL_FLAG_PASSWORD + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_CODEC_IS_UNENCRYPTED = (CHANNEL_CODEC_LATENCY_FACTOR + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

CHANNEL_ENDMARKER = (CHANNEL_CODEC_IS_UNENCRYPTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 105

enum_ClientProperties = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_UNIQUE_IDENTIFIER = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_NICKNAME = (CLIENT_UNIQUE_IDENTIFIER + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_VERSION = (CLIENT_NICKNAME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_PLATFORM = (CLIENT_VERSION + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_FLAG_TALKING = (CLIENT_PLATFORM + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_INPUT_MUTED = (CLIENT_FLAG_TALKING + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_OUTPUT_MUTED = (CLIENT_INPUT_MUTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_OUTPUTONLY_MUTED = (CLIENT_OUTPUT_MUTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_INPUT_HARDWARE = (CLIENT_OUTPUTONLY_MUTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_OUTPUT_HARDWARE = (CLIENT_INPUT_HARDWARE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_INPUT_DEACTIVATED = (CLIENT_OUTPUT_HARDWARE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_IDLE_TIME = (CLIENT_INPUT_DEACTIVATED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_DEFAULT_CHANNEL = (CLIENT_IDLE_TIME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_DEFAULT_CHANNEL_PASSWORD = (CLIENT_DEFAULT_CHANNEL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_SERVER_PASSWORD = (CLIENT_DEFAULT_CHANNEL_PASSWORD + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_META_DATA = (CLIENT_SERVER_PASSWORD + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_IS_MUTED = (CLIENT_META_DATA + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_IS_RECORDING = (CLIENT_IS_MUTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_VOLUME_MODIFICATOR = (CLIENT_IS_RECORDING + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

CLIENT_ENDMARKER = (CLIENT_VOLUME_MODIFICATOR + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 124

enum_VirtualServerProperties = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_UNIQUE_IDENTIFIER = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_NAME = (VIRTUALSERVER_UNIQUE_IDENTIFIER + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_WELCOMEMESSAGE = (VIRTUALSERVER_NAME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_PLATFORM = (VIRTUALSERVER_WELCOMEMESSAGE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_VERSION = (VIRTUALSERVER_PLATFORM + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_MAXCLIENTS = (VIRTUALSERVER_VERSION + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_PASSWORD = (VIRTUALSERVER_MAXCLIENTS + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_CLIENTS_ONLINE = (VIRTUALSERVER_PASSWORD + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_CHANNELS_ONLINE = (VIRTUALSERVER_CLIENTS_ONLINE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_CREATED = (VIRTUALSERVER_CHANNELS_ONLINE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_UPTIME = (VIRTUALSERVER_CREATED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_CODEC_ENCRYPTION_MODE = (VIRTUALSERVER_UPTIME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

VIRTUALSERVER_ENDMARKER = (VIRTUALSERVER_CODEC_ENCRYPTION_MODE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 147

enum_ConnectionProperties = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PING = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PING_DEVIATION = (CONNECTION_PING + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CONNECTED_TIME = (CONNECTION_PING_DEVIATION + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_IDLE_TIME = (CONNECTION_CONNECTED_TIME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CLIENT_IP = (CONNECTION_IDLE_TIME + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CLIENT_PORT = (CONNECTION_CLIENT_IP + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_SERVER_IP = (CONNECTION_CLIENT_PORT + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_SERVER_PORT = (CONNECTION_SERVER_IP + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_SENT_SPEECH = (CONNECTION_SERVER_PORT + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_SENT_KEEPALIVE = (CONNECTION_PACKETS_SENT_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_SENT_CONTROL = (CONNECTION_PACKETS_SENT_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_SENT_TOTAL = (CONNECTION_PACKETS_SENT_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_SENT_SPEECH = (CONNECTION_PACKETS_SENT_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_SENT_KEEPALIVE = (CONNECTION_BYTES_SENT_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_SENT_CONTROL = (CONNECTION_BYTES_SENT_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_SENT_TOTAL = (CONNECTION_BYTES_SENT_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_RECEIVED_SPEECH = (CONNECTION_BYTES_SENT_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_RECEIVED_KEEPALIVE = (CONNECTION_PACKETS_RECEIVED_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_RECEIVED_CONTROL = (CONNECTION_PACKETS_RECEIVED_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETS_RECEIVED_TOTAL = (CONNECTION_PACKETS_RECEIVED_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_RECEIVED_SPEECH = (CONNECTION_PACKETS_RECEIVED_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_RECEIVED_KEEPALIVE = (CONNECTION_BYTES_RECEIVED_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_RECEIVED_CONTROL = (CONNECTION_BYTES_RECEIVED_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BYTES_RECEIVED_TOTAL = (CONNECTION_BYTES_RECEIVED_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETLOSS_SPEECH = (CONNECTION_BYTES_RECEIVED_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETLOSS_KEEPALIVE = (CONNECTION_PACKETLOSS_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETLOSS_CONTROL = (CONNECTION_PACKETLOSS_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_PACKETLOSS_TOTAL = (CONNECTION_PACKETLOSS_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_SERVER2CLIENT_PACKETLOSS_SPEECH = (CONNECTION_PACKETLOSS_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_SERVER2CLIENT_PACKETLOSS_KEEPALIVE = (CONNECTION_SERVER2CLIENT_PACKETLOSS_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_SERVER2CLIENT_PACKETLOSS_CONTROL = (CONNECTION_SERVER2CLIENT_PACKETLOSS_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_SERVER2CLIENT_PACKETLOSS_TOTAL = (CONNECTION_SERVER2CLIENT_PACKETLOSS_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CLIENT2SERVER_PACKETLOSS_SPEECH = (CONNECTION_SERVER2CLIENT_PACKETLOSS_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CLIENT2SERVER_PACKETLOSS_KEEPALIVE = (CONNECTION_CLIENT2SERVER_PACKETLOSS_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CLIENT2SERVER_PACKETLOSS_CONTROL = (CONNECTION_CLIENT2SERVER_PACKETLOSS_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_CLIENT2SERVER_PACKETLOSS_TOTAL = (CONNECTION_CLIENT2SERVER_PACKETLOSS_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_SECOND_SPEECH = (CONNECTION_CLIENT2SERVER_PACKETLOSS_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_SECOND_KEEPALIVE = (CONNECTION_BANDWIDTH_SENT_LAST_SECOND_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_SECOND_CONTROL = (CONNECTION_BANDWIDTH_SENT_LAST_SECOND_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_SECOND_TOTAL = (CONNECTION_BANDWIDTH_SENT_LAST_SECOND_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_SPEECH = (CONNECTION_BANDWIDTH_SENT_LAST_SECOND_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_KEEPALIVE = (CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_CONTROL = (CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_TOTAL = (CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_SPEECH = (CONNECTION_BANDWIDTH_SENT_LAST_MINUTE_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_KEEPALIVE = (CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_CONTROL = (CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_TOTAL = (CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_SPEECH = (CONNECTION_BANDWIDTH_RECEIVED_LAST_SECOND_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_KEEPALIVE = (CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_SPEECH + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_CONTROL = (CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_KEEPALIVE + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_TOTAL = (CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_CONTROL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

CONNECTION_ENDMARKER = (CONNECTION_BANDWIDTH_RECEIVED_LAST_MINUTE_TOTAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 163

enum_LogTypes = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

LogType_NONE = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

LogType_FILE = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

LogType_CONSOLE = 2 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

LogType_USERLOGGING = 4 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

LogType_NO_NETLOGGING = 8 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

LogType_DATABASE = 16 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 219

enum_LogLevel = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

LogLevel_CRITICAL = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

LogLevel_ERROR = (LogLevel_CRITICAL + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

LogLevel_WARNING = (LogLevel_ERROR + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

LogLevel_DEBUG = (LogLevel_WARNING + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

LogLevel_INFO = (LogLevel_DEBUG + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

LogLevel_DEVEL = (LogLevel_INFO + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 241
class struct_anon_1(Structure):
    pass

struct_anon_1.__slots__ = [
    'x',
    'y',
    'z',
]
struct_anon_1._fields_ = [
    ('x', c_float),
    ('y', c_float),
    ('z', c_float),
]

TS3_VECTOR = struct_anon_1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 241

enum_GroupWhisperType = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 243

GROUPWHISPERTYPE_SERVERGROUP = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 243

GROUPWHISPERTYPE_CHANNELGROUP = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 243

GROUPWHISPERTYPE_CHANNELCOMMANDER = 2 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 243

GROUPWHISPERTYPE_ALLCLIENTS = 3 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 243

GROUPWHISPERTYPE_ENDMARKER = (GROUPWHISPERTYPE_ALLCLIENTS + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 243

enum_GroupWhisperTargetMode = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_ALL = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_CURRENTCHANNEL = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_PARENTCHANNEL = 2 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_ALLPARENTCHANNELS = 3 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_CHANNELFAMILY = 4 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_ANCESTORCHANNELFAMILY = 5 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_SUBCHANNELS = 6 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

GROUPWHISPERTARGETMODE_ENDMARKER = (GROUPWHISPERTARGETMODE_SUBCHANNELS + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 251

enum_MonoSoundDestination = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 262

MONO_SOUND_DESTINATION_ALL = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 262

MONO_SOUND_DESTINATION_FRONT_CENTER = 1 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 262

MONO_SOUND_DESTINATION_FRONT_LEFT_AND_RIGHT = 2 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 262

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 5
try:
    TS3_MAX_SIZE_CHANNEL_NAME = 40
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 6
try:
    TS3_MAX_SIZE_VIRTUALSERVER_NAME = 64
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 7
try:
    TS3_MAX_SIZE_CLIENT_NICKNAME = 64
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 8
try:
    TS3_MIN_SIZE_CLIENT_NICKNAME = 3
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 9
try:
    TS3_MAX_SIZE_REASON_MESSAGE = 80
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 12
try:
    TS3_MAX_SIZE_TEXTMESSAGE = 1024
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 13
try:
    TS3_MAX_SIZE_CHANNEL_TOPIC = 255
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 14
try:
    TS3_MAX_SIZE_CHANNEL_DESCRIPTION = 8192
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 15
try:
    TS3_MAX_SIZE_VIRTUALSERVER_WELCOMEMESSAGE = 1024
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 18
try:
    TS3_MIN_SECONDS_CLIENTID_REUSE = 300
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 270
try:
    SPEAKER_FRONT_LEFT = 1
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 271
try:
    SPEAKER_FRONT_RIGHT = 2
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 272
try:
    SPEAKER_FRONT_CENTER = 4
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 273
try:
    SPEAKER_LOW_FREQUENCY = 8
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 274
try:
    SPEAKER_BACK_LEFT = 16
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 275
try:
    SPEAKER_BACK_RIGHT = 32
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 276
try:
    SPEAKER_FRONT_LEFT_OF_CENTER = 64
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 277
try:
    SPEAKER_FRONT_RIGHT_OF_CENTER = 128
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 278
try:
    SPEAKER_BACK_CENTER = 256
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 279
try:
    SPEAKER_SIDE_LEFT = 512
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 280
try:
    SPEAKER_SIDE_RIGHT = 1024
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 281
try:
    SPEAKER_TOP_CENTER = 2048
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 282
try:
    SPEAKER_TOP_FRONT_LEFT = 4096
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 283
try:
    SPEAKER_TOP_FRONT_CENTER = 8192
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 284
try:
    SPEAKER_TOP_FRONT_RIGHT = 16384
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 285
try:
    SPEAKER_TOP_BACK_LEFT = 32768
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 286
try:
    SPEAKER_TOP_BACK_CENTER = 65536
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 287
try:
    SPEAKER_TOP_BACK_RIGHT = 131072
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 289
try:
    SPEAKER_HEADPHONES_LEFT = 268435456
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 290
try:
    SPEAKER_HEADPHONES_RIGHT = 536870912
except:
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 291
try:
    SPEAKER_MONO = 1073741824
except:
    pass

# Other definitions
enum_Visibility = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 4

ENTER_VISIBILITY = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 4

RETAIN_VISIBILITY = (ENTER_VISIBILITY + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 4

LEAVE_VISIBILITY = (RETAIN_VISIBILITY + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 4

enum_ConnectStatus = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 10

STATUS_DISCONNECTED = 0 # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 10

STATUS_CONNECTING = (STATUS_DISCONNECTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 10

STATUS_CONNECTED = (STATUS_CONNECTING + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 10

STATUS_CONNECTION_ESTABLISHING = (STATUS_CONNECTED + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 10

STATUS_CONNECTION_ESTABLISHED = (STATUS_CONNECTION_ESTABLISHING + 1) # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib_publicdefinitions.h: 10

anyID = c_uint16 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 30

uint64 = c_uint64 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 31

enum_LogLevel = c_int # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 228

# /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 241
class struct_anon_22(Structure):
    pass

struct_anon_22.__slots__ = [
    'x',
    'y',
    'z',
]
struct_anon_22._fields_ = [
    ('x', c_float),
    ('y', c_float),
    ('z', c_float),
]

TS3_VECTOR = struct_anon_22 # /home/mike/ts3/ts3_sdk_3.0.2/include/public_definitions.h: 241


# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 23
class struct_ClientUIFunctionsRare(Structure):
    pass

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 25
class struct_ClientUIFunctions(Structure):
    pass

struct_ClientUIFunctions.__slots__ = [
    'onConnectStatusChangeEvent',
    'onServerProtocolVersionEvent',
    'onNewChannelEvent',
    'onNewChannelCreatedEvent',
    'onDelChannelEvent',
    'onChannelMoveEvent',
    'onUpdateChannelEvent',
    'onUpdateChannelEditedEvent',
    'onUpdateClientEvent',
    'onClientMoveEvent',
    'onClientMoveSubscriptionEvent',
    'onClientMoveTimeoutEvent',
    'onClientMoveMovedEvent',
    'onClientKickFromChannelEvent',
    'onClientKickFromServerEvent',
    'onClientIDsEvent',
    'onClientIDsFinishedEvent',
    'onServerEditedEvent',
    'onServerUpdatedEvent',
    'onServerErrorEvent',
    'onServerStopEvent',
    'onTextMessageEvent',
    'onTalkStatusChangeEvent',
    'onIgnoredWhisperEvent',
    'onConnectionInfoEvent',
    'onServerConnectionInfoEvent',
    'onChannelSubscribeEvent',
    'onChannelSubscribeFinishedEvent',
    'onChannelUnsubscribeEvent',
    'onChannelUnsubscribeFinishedEvent',
    'onChannelDescriptionUpdateEvent',
    'onChannelPasswordChangedEvent',
    'onPlaybackShutdownCompleteEvent',
    'onSoundDeviceListChangedEvent',
    'onEditPlaybackVoiceDataEvent',
    'onEditPostProcessVoiceDataEvent',
    'onEditMixedPlaybackVoiceDataEvent',
    'onEditCapturedVoiceDataEvent',
    'onCustom3dRolloffCalculationClientEvent',
    'onCustom3dRolloffCalculationWaveEvent',
    'onUserLoggingMessageEvent',
    'onCustomPacketEncryptEvent',
    'onCustomPacketDecryptEvent',
    'onProvisioningSlotRequestResultEvent',
]
struct_ClientUIFunctions._fields_ = [
    ('onConnectStatusChangeEvent', CFUNCTYPE(None, uint64, c_int, c_uint)),
    ('onServerProtocolVersionEvent', CFUNCTYPE(None, uint64, c_int)),
    ('onNewChannelEvent', CFUNCTYPE(None, uint64, uint64, uint64)),
    ('onNewChannelCreatedEvent', CFUNCTYPE(None, uint64, uint64, uint64, anyID, POINTER(c_char), POINTER(c_char))),
    ('onDelChannelEvent', CFUNCTYPE(None, uint64, uint64, anyID, POINTER(c_char), POINTER(c_char))),
    ('onChannelMoveEvent', CFUNCTYPE(None, uint64, uint64, uint64, anyID, POINTER(c_char), POINTER(c_char))),
    ('onUpdateChannelEvent', CFUNCTYPE(None, uint64, uint64)),
    ('onUpdateChannelEditedEvent', CFUNCTYPE(None, uint64, uint64, anyID, POINTER(c_char), POINTER(c_char))),
    ('onUpdateClientEvent', CFUNCTYPE(None, uint64, anyID, anyID, POINTER(c_char), POINTER(c_char))),
    ('onClientMoveEvent', CFUNCTYPE(None, uint64, anyID, uint64, uint64, c_int, POINTER(c_char))),
    ('onClientMoveSubscriptionEvent', CFUNCTYPE(None, uint64, anyID, uint64, uint64, c_int)),
    ('onClientMoveTimeoutEvent', CFUNCTYPE(None, uint64, anyID, uint64, uint64, c_int, POINTER(c_char))),
    ('onClientMoveMovedEvent', CFUNCTYPE(None, uint64, anyID, uint64, uint64, c_int, anyID, POINTER(c_char), POINTER(c_char), POINTER(c_char))),
    ('onClientKickFromChannelEvent', CFUNCTYPE(None, uint64, anyID, uint64, uint64, c_int, anyID, POINTER(c_char), POINTER(c_char), POINTER(c_char))),
    ('onClientKickFromServerEvent', CFUNCTYPE(None, uint64, anyID, uint64, uint64, c_int, anyID, POINTER(c_char), POINTER(c_char), POINTER(c_char))),
    ('onClientIDsEvent', CFUNCTYPE(None, uint64, POINTER(c_char), anyID, POINTER(c_char))),
    ('onClientIDsFinishedEvent', CFUNCTYPE(None, uint64)),
    ('onServerEditedEvent', CFUNCTYPE(None, uint64, anyID, POINTER(c_char), POINTER(c_char))),
    ('onServerUpdatedEvent', CFUNCTYPE(None, uint64)),
    ('onServerErrorEvent', CFUNCTYPE(None, uint64, POINTER(c_char), c_uint, POINTER(c_char), POINTER(c_char))),
    ('onServerStopEvent', CFUNCTYPE(None, uint64, POINTER(c_char))),
    ('onTextMessageEvent', CFUNCTYPE(None, uint64, anyID, anyID, anyID, POINTER(c_char), POINTER(c_char), POINTER(c_char))),
    ('onTalkStatusChangeEvent', CFUNCTYPE(None, uint64, c_int, c_int, anyID)),
    ('onIgnoredWhisperEvent', CFUNCTYPE(None, uint64, anyID)),
    ('onConnectionInfoEvent', CFUNCTYPE(None, uint64, anyID)),
    ('onServerConnectionInfoEvent', CFUNCTYPE(None, uint64)),
    ('onChannelSubscribeEvent', CFUNCTYPE(None, uint64, uint64)),
    ('onChannelSubscribeFinishedEvent', CFUNCTYPE(None, uint64)),
    ('onChannelUnsubscribeEvent', CFUNCTYPE(None, uint64, uint64)),
    ('onChannelUnsubscribeFinishedEvent', CFUNCTYPE(None, uint64)),
    ('onChannelDescriptionUpdateEvent', CFUNCTYPE(None, uint64, uint64)),
    ('onChannelPasswordChangedEvent', CFUNCTYPE(None, uint64, uint64)),
    ('onPlaybackShutdownCompleteEvent', CFUNCTYPE(None, uint64)),
    ('onSoundDeviceListChangedEvent', CFUNCTYPE(None, POINTER(c_char), c_int)),
    ('onEditPlaybackVoiceDataEvent', CFUNCTYPE(None, uint64, anyID, POINTER(c_short), c_int, c_int)),
    ('onEditPostProcessVoiceDataEvent', CFUNCTYPE(None, uint64, anyID, POINTER(c_short), c_int, c_int, POINTER(c_uint), POINTER(c_uint))),
    ('onEditMixedPlaybackVoiceDataEvent', CFUNCTYPE(None, uint64, POINTER(c_short), c_int, c_int, POINTER(c_uint), POINTER(c_uint))),
    ('onEditCapturedVoiceDataEvent', CFUNCTYPE(None, uint64, POINTER(c_short), c_int, c_int, POINTER(c_int))),
    ('onCustom3dRolloffCalculationClientEvent', CFUNCTYPE(None, uint64, anyID, c_float, POINTER(c_float))),
    ('onCustom3dRolloffCalculationWaveEvent', CFUNCTYPE(None, uint64, uint64, c_float, POINTER(c_float))),
    ('onUserLoggingMessageEvent', CFUNCTYPE(None, POINTER(c_char), c_int, POINTER(c_char), uint64, POINTER(c_char), POINTER(c_char))),
    ('onCustomPacketEncryptEvent', CFUNCTYPE(None, POINTER(POINTER(c_char)), POINTER(c_uint))),
    ('onCustomPacketDecryptEvent', CFUNCTYPE(None, POINTER(POINTER(c_char)), POINTER(c_uint))),
    ('onProvisioningSlotRequestResultEvent', CFUNCTYPE(None, c_uint, uint64, POINTER(c_char))),
]

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 74
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_freeMemory'):
        continue
    ts3client_freeMemory = _lib.ts3client_freeMemory
    ts3client_freeMemory.argtypes = [POINTER(None)]
    ts3client_freeMemory.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 77
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_initClientLib'):
        continue
    ts3client_initClientLib = _lib.ts3client_initClientLib
    ts3client_initClientLib.argtypes = [POINTER(struct_ClientUIFunctions), POINTER(struct_ClientUIFunctionsRare), c_int, c_char_p, c_char_p]
    ts3client_initClientLib.restype = c_uint
    ts3client_initClientLib.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 78
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_destroyClientLib'):
        continue
    ts3client_destroyClientLib = _lib.ts3client_destroyClientLib
    ts3client_destroyClientLib.argtypes = []
    ts3client_destroyClientLib.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 79
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientLibVersion'):
        continue
    ts3client_getClientLibVersion = _lib.ts3client_getClientLibVersion
    ts3client_getClientLibVersion.argtypes = [POINTER(POINTER(c_char))]
    ts3client_getClientLibVersion.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 80
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientLibVersionNumber'):
        continue
    ts3client_getClientLibVersionNumber = _lib.ts3client_getClientLibVersionNumber
    ts3client_getClientLibVersionNumber.argtypes = [POINTER(uint64)]
    ts3client_getClientLibVersionNumber.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 82
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_spawnNewServerConnectionHandler'):
        continue
    ts3client_spawnNewServerConnectionHandler = _lib.ts3client_spawnNewServerConnectionHandler
    ts3client_spawnNewServerConnectionHandler.argtypes = [c_int, POINTER(uint64)]
    ts3client_spawnNewServerConnectionHandler.restype = c_uint
    ts3client_spawnNewServerConnectionHandler.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 83
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_destroyServerConnectionHandler'):
        continue
    ts3client_destroyServerConnectionHandler = _lib.ts3client_destroyServerConnectionHandler
    ts3client_destroyServerConnectionHandler.argtypes = [uint64]
    ts3client_destroyServerConnectionHandler.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 86
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_createIdentity'):
        continue
    ts3client_createIdentity = _lib.ts3client_createIdentity
    ts3client_createIdentity.argtypes = [POINTER(c_char_p)]
    ts3client_createIdentity.restype = c_uint
    ts3client_createIdentity.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 89
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getPlaybackDeviceList'):
        continue
    ts3client_getPlaybackDeviceList = _lib.ts3client_getPlaybackDeviceList
    ts3client_getPlaybackDeviceList.argtypes = [POINTER(c_char), POINTER(POINTER(POINTER(POINTER(c_char))))]
    ts3client_getPlaybackDeviceList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 90
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getCaptureDeviceList'):
        continue
    ts3client_getCaptureDeviceList = _lib.ts3client_getCaptureDeviceList
    ts3client_getCaptureDeviceList.argtypes = [POINTER(c_char), POINTER(POINTER(POINTER(POINTER(c_char))))]
    ts3client_getCaptureDeviceList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 91
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getPlaybackModeList'):
        continue
    ts3client_getPlaybackModeList = _lib.ts3client_getPlaybackModeList
    ts3client_getPlaybackModeList.argtypes = [POINTER(POINTER(POINTER(c_char)))]
    ts3client_getPlaybackModeList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 92
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getCaptureModeList'):
        continue
    ts3client_getCaptureModeList = _lib.ts3client_getCaptureModeList
    ts3client_getCaptureModeList.argtypes = [POINTER(POINTER(POINTER(c_char)))]
    ts3client_getCaptureModeList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 93
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getDefaultPlaybackDevice'):
        continue
    ts3client_getDefaultPlaybackDevice = _lib.ts3client_getDefaultPlaybackDevice
    ts3client_getDefaultPlaybackDevice.argtypes = [POINTER(c_char), POINTER(POINTER(POINTER(c_char)))]
    ts3client_getDefaultPlaybackDevice.restype = c_uint
    ts3client_getDefaultPlaybackDevice.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 94
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getDefaultCaptureDevice'):
        continue
    ts3client_getDefaultCaptureDevice = _lib.ts3client_getDefaultCaptureDevice
    ts3client_getDefaultCaptureDevice.argtypes = [POINTER(c_char), POINTER(POINTER(POINTER(c_char)))]
    ts3client_getDefaultCaptureDevice.restype = c_uint
    ts3client_getDefaultCaptureDevice.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 95
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getDefaultPlayBackMode'):
        continue
    ts3client_getDefaultPlayBackMode = _lib.ts3client_getDefaultPlayBackMode
    ts3client_getDefaultPlayBackMode.argtypes = [POINTER(c_char_p)]
    ts3client_getDefaultPlayBackMode.restype = c_uint
    ts3client_getDefaultPlayBackMode.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 96
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getDefaultCaptureMode'):
        continue
    ts3client_getDefaultCaptureMode = _lib.ts3client_getDefaultCaptureMode
    ts3client_getDefaultCaptureMode.argtypes = [POINTER(c_char_p)]
    ts3client_getDefaultCaptureMode.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 98
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_openPlaybackDevice'):
        continue
    ts3client_openPlaybackDevice = _lib.ts3client_openPlaybackDevice
    ts3client_openPlaybackDevice.argtypes = [uint64, c_char_p, c_char_p]
    ts3client_openPlaybackDevice.restype = c_uint
    ts3client_openPlaybackDevice.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 99
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_openCaptureDevice'):
        continue
    ts3client_openCaptureDevice = _lib.ts3client_openCaptureDevice
    ts3client_openCaptureDevice.argtypes = [uint64, c_char_p, c_char_p]
    ts3client_openCaptureDevice.restype = c_uint
    ts3client_openCaptureDevice.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 100
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getCurrentPlaybackDeviceName'):
        continue
    ts3client_getCurrentPlaybackDeviceName = _lib.ts3client_getCurrentPlaybackDeviceName
    ts3client_getCurrentPlaybackDeviceName.argtypes = [uint64, POINTER(POINTER(c_char)), POINTER(c_int)]
    ts3client_getCurrentPlaybackDeviceName.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 101
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getCurrentPlayBackMode'):
        continue
    ts3client_getCurrentPlayBackMode = _lib.ts3client_getCurrentPlayBackMode
    ts3client_getCurrentPlayBackMode.argtypes = [uint64, POINTER(POINTER(c_char))]
    ts3client_getCurrentPlayBackMode.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 102
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getCurrentCaptureDeviceName'):
        continue
    ts3client_getCurrentCaptureDeviceName = _lib.ts3client_getCurrentCaptureDeviceName
    ts3client_getCurrentCaptureDeviceName.argtypes = [uint64, POINTER(POINTER(c_char)), POINTER(c_int)]
    ts3client_getCurrentCaptureDeviceName.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 103
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getCurrentCaptureMode'):
        continue
    ts3client_getCurrentCaptureMode = _lib.ts3client_getCurrentCaptureMode
    ts3client_getCurrentCaptureMode.argtypes = [uint64, POINTER(POINTER(c_char))]
    ts3client_getCurrentCaptureMode.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 104
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_initiateGracefulPlaybackShutdown'):
        continue
    ts3client_initiateGracefulPlaybackShutdown = _lib.ts3client_initiateGracefulPlaybackShutdown
    ts3client_initiateGracefulPlaybackShutdown.argtypes = [uint64]
    ts3client_initiateGracefulPlaybackShutdown.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 105
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_closePlaybackDevice'):
        continue
    ts3client_closePlaybackDevice = _lib.ts3client_closePlaybackDevice
    ts3client_closePlaybackDevice.argtypes = [uint64]
    ts3client_closePlaybackDevice.restype = c_uint
    ts3client_closePlaybackDevice.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 106
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_closeCaptureDevice'):
        continue
    ts3client_closeCaptureDevice = _lib.ts3client_closeCaptureDevice
    ts3client_closeCaptureDevice.argtypes = [uint64]
    ts3client_closeCaptureDevice.restype = c_uint
    ts3client_closeCaptureDevice.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 107
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_activateCaptureDevice'):
        continue
    ts3client_activateCaptureDevice = _lib.ts3client_activateCaptureDevice
    ts3client_activateCaptureDevice.argtypes = [uint64]
    ts3client_activateCaptureDevice.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 109
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_playWaveFile'):
        continue
    ts3client_playWaveFile = _lib.ts3client_playWaveFile
    ts3client_playWaveFile.argtypes = [uint64, POINTER(c_char)]
    ts3client_playWaveFile.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 110
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_playWaveFileHandle'):
        continue
    ts3client_playWaveFileHandle = _lib.ts3client_playWaveFileHandle
    ts3client_playWaveFileHandle.argtypes = [uint64, POINTER(c_char), c_int, POINTER(uint64)]
    ts3client_playWaveFileHandle.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 111
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_pauseWaveFileHandle'):
        continue
    ts3client_pauseWaveFileHandle = _lib.ts3client_pauseWaveFileHandle
    ts3client_pauseWaveFileHandle.argtypes = [uint64, uint64, c_int]
    ts3client_pauseWaveFileHandle.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 112
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_closeWaveFileHandle'):
        continue
    ts3client_closeWaveFileHandle = _lib.ts3client_closeWaveFileHandle
    ts3client_closeWaveFileHandle.argtypes = [uint64, uint64]
    ts3client_closeWaveFileHandle.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 114
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_registerCustomDevice'):
        continue
    ts3client_registerCustomDevice = _lib.ts3client_registerCustomDevice
    ts3client_registerCustomDevice.argtypes = [POINTER(c_char), POINTER(c_char), c_int, c_int, c_int, c_int]
    ts3client_registerCustomDevice.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 115
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_unregisterCustomDevice'):
        continue
    ts3client_unregisterCustomDevice = _lib.ts3client_unregisterCustomDevice
    ts3client_unregisterCustomDevice.argtypes = [POINTER(c_char)]
    ts3client_unregisterCustomDevice.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 116
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_processCustomCaptureData'):
        continue
    ts3client_processCustomCaptureData = _lib.ts3client_processCustomCaptureData
    ts3client_processCustomCaptureData.argtypes = [POINTER(c_char), POINTER(c_short), c_int]
    ts3client_processCustomCaptureData.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 117
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_acquireCustomPlaybackData'):
        continue
    ts3client_acquireCustomPlaybackData = _lib.ts3client_acquireCustomPlaybackData
    ts3client_acquireCustomPlaybackData.argtypes = [POINTER(c_char), POINTER(c_short), c_int]
    ts3client_acquireCustomPlaybackData.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 119
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setLocalTestMode'):
        continue
    ts3client_setLocalTestMode = _lib.ts3client_setLocalTestMode
    ts3client_setLocalTestMode.argtypes = [uint64, c_int]
    ts3client_setLocalTestMode.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 121
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_startVoiceRecording'):
        continue
    ts3client_startVoiceRecording = _lib.ts3client_startVoiceRecording
    ts3client_startVoiceRecording.argtypes = [uint64]
    ts3client_startVoiceRecording.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 122
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_stopVoiceRecording'):
        continue
    ts3client_stopVoiceRecording = _lib.ts3client_stopVoiceRecording
    ts3client_stopVoiceRecording.argtypes = [uint64]
    ts3client_stopVoiceRecording.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 124
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_allowWhispersFrom'):
        continue
    ts3client_allowWhispersFrom = _lib.ts3client_allowWhispersFrom
    ts3client_allowWhispersFrom.argtypes = [uint64, anyID]
    ts3client_allowWhispersFrom.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 125
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_removeFromAllowedWhispersFrom'):
        continue
    ts3client_removeFromAllowedWhispersFrom = _lib.ts3client_removeFromAllowedWhispersFrom
    ts3client_removeFromAllowedWhispersFrom.argtypes = [uint64, anyID]
    ts3client_removeFromAllowedWhispersFrom.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 128
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_systemset3DListenerAttributes'):
        continue
    ts3client_systemset3DListenerAttributes = _lib.ts3client_systemset3DListenerAttributes
    ts3client_systemset3DListenerAttributes.argtypes = [uint64, POINTER(TS3_VECTOR), POINTER(TS3_VECTOR), POINTER(TS3_VECTOR)]
    ts3client_systemset3DListenerAttributes.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 129
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_set3DWaveAttributes'):
        continue
    ts3client_set3DWaveAttributes = _lib.ts3client_set3DWaveAttributes
    ts3client_set3DWaveAttributes.argtypes = [uint64, uint64, POINTER(TS3_VECTOR)]
    ts3client_set3DWaveAttributes.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 130
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_systemset3DSettings'):
        continue
    ts3client_systemset3DSettings = _lib.ts3client_systemset3DSettings
    ts3client_systemset3DSettings.argtypes = [uint64, c_float, c_float]
    ts3client_systemset3DSettings.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 131
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_channelset3DAttributes'):
        continue
    ts3client_channelset3DAttributes = _lib.ts3client_channelset3DAttributes
    ts3client_channelset3DAttributes.argtypes = [uint64, anyID, POINTER(TS3_VECTOR)]
    ts3client_channelset3DAttributes.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 134
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getPreProcessorInfoValueFloat'):
        continue
    ts3client_getPreProcessorInfoValueFloat = _lib.ts3client_getPreProcessorInfoValueFloat
    ts3client_getPreProcessorInfoValueFloat.argtypes = [uint64, POINTER(c_char), POINTER(c_float)]
    ts3client_getPreProcessorInfoValueFloat.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 135
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getPreProcessorConfigValue'):
        continue
    ts3client_getPreProcessorConfigValue = _lib.ts3client_getPreProcessorConfigValue
    ts3client_getPreProcessorConfigValue.argtypes = [uint64, POINTER(c_char), POINTER(POINTER(c_char))]
    ts3client_getPreProcessorConfigValue.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 136
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setPreProcessorConfigValue'):
        continue
    ts3client_setPreProcessorConfigValue = _lib.ts3client_setPreProcessorConfigValue
    ts3client_setPreProcessorConfigValue.argtypes = [uint64, POINTER(c_char), POINTER(c_char)]
    ts3client_setPreProcessorConfigValue.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 139
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getEncodeConfigValue'):
        continue
    ts3client_getEncodeConfigValue = _lib.ts3client_getEncodeConfigValue
    ts3client_getEncodeConfigValue.argtypes = [uint64, POINTER(c_char), POINTER(POINTER(c_char))]
    ts3client_getEncodeConfigValue.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 142
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getPlaybackConfigValueAsFloat'):
        continue
    ts3client_getPlaybackConfigValueAsFloat = _lib.ts3client_getPlaybackConfigValueAsFloat
    ts3client_getPlaybackConfigValueAsFloat.argtypes = [uint64, c_char_p, POINTER(c_float)]
    ts3client_getPlaybackConfigValueAsFloat.restype = c_uint
    ts3client_getPlaybackConfigValueAsFloat.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 143
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setPlaybackConfigValue'):
        continue
    ts3client_setPlaybackConfigValue = _lib.ts3client_setPlaybackConfigValue
    ts3client_setPlaybackConfigValue.argtypes = [uint64, c_char_p, c_char_p]
    ts3client_setPlaybackConfigValue.restype = c_uint
    ts3client_setPlaybackConfigValue.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 144
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setClientVolumeModifier'):
        continue
    ts3client_setClientVolumeModifier = _lib.ts3client_setClientVolumeModifier
    ts3client_setClientVolumeModifier.argtypes = [uint64, anyID, c_float]
    ts3client_setClientVolumeModifier.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 147
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_logMessage'):
        continue
    ts3client_logMessage = _lib.ts3client_logMessage
    ts3client_logMessage.argtypes = [POINTER(c_char), enum_LogLevel, POINTER(c_char), uint64]
    ts3client_logMessage.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 148
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setLogVerbosity'):
        continue
    ts3client_setLogVerbosity = _lib.ts3client_setLogVerbosity
    ts3client_setLogVerbosity.argtypes = [enum_LogLevel]
    ts3client_setLogVerbosity.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 151
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getErrorMessage'):
        continue
    ts3client_getErrorMessage = _lib.ts3client_getErrorMessage
    ts3client_getErrorMessage.argtypes = [c_uint, POINTER(POINTER(c_char))]
    ts3client_getErrorMessage.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 154
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_startConnection'):
        continue
    ts3client_startConnection = _lib.ts3client_startConnection
    ts3client_startConnection.argtypes = [uint64, c_char_p, c_char_p, c_uint, c_char_p, POINTER(c_char_p), c_char_p, c_char_p]
    ts3client_startConnection.restype = c_uint
    ts3client_startConnection.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 156
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_stopConnection'):
        continue
    ts3client_stopConnection = _lib.ts3client_stopConnection
    ts3client_stopConnection.argtypes = [uint64, POINTER(c_char)]
    ts3client_stopConnection.restype = c_uint
    ts3client_stopConnection.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 157
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestClientMove'):
        continue
    ts3client_requestClientMove = _lib.ts3client_requestClientMove
    ts3client_requestClientMove.argtypes = [uint64, anyID, uint64, c_char_p, c_char_p]
    ts3client_requestClientMove.restype = c_uint
    ts3client_requestClientMove.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 158
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestClientVariables'):
        continue
    ts3client_requestClientVariables = _lib.ts3client_requestClientVariables
    ts3client_requestClientVariables.argtypes = [uint64, anyID, POINTER(c_char)]
    ts3client_requestClientVariables.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 159
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestClientKickFromChannel'):
        continue
    ts3client_requestClientKickFromChannel = _lib.ts3client_requestClientKickFromChannel
    ts3client_requestClientKickFromChannel.argtypes = [uint64, anyID, POINTER(c_char), POINTER(c_char)]
    ts3client_requestClientKickFromChannel.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 160
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestClientKickFromServer'):
        continue
    ts3client_requestClientKickFromServer = _lib.ts3client_requestClientKickFromServer
    ts3client_requestClientKickFromServer.argtypes = [uint64, anyID, POINTER(c_char), POINTER(c_char)]
    ts3client_requestClientKickFromServer.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 161
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelDelete'):
        continue
    ts3client_requestChannelDelete = _lib.ts3client_requestChannelDelete
    ts3client_requestChannelDelete.argtypes = [uint64, uint64, c_int, POINTER(c_char)]
    ts3client_requestChannelDelete.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 162
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelMove'):
        continue
    ts3client_requestChannelMove = _lib.ts3client_requestChannelMove
    ts3client_requestChannelMove.argtypes = [uint64, uint64, uint64, uint64, POINTER(c_char)]
    ts3client_requestChannelMove.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 163
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestSendPrivateTextMsg'):
        continue
    ts3client_requestSendPrivateTextMsg = _lib.ts3client_requestSendPrivateTextMsg
    ts3client_requestSendPrivateTextMsg.argtypes = [uint64, POINTER(c_char), anyID, POINTER(c_char)]
    ts3client_requestSendPrivateTextMsg.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 164
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestSendChannelTextMsg'):
        continue
    ts3client_requestSendChannelTextMsg = _lib.ts3client_requestSendChannelTextMsg
    ts3client_requestSendChannelTextMsg.argtypes = [uint64, POINTER(c_char), uint64, POINTER(c_char)]
    ts3client_requestSendChannelTextMsg.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 165
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestSendServerTextMsg'):
        continue
    ts3client_requestSendServerTextMsg = _lib.ts3client_requestSendServerTextMsg
    ts3client_requestSendServerTextMsg.argtypes = [uint64, POINTER(c_char), POINTER(c_char)]
    ts3client_requestSendServerTextMsg.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 166
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestConnectionInfo'):
        continue
    ts3client_requestConnectionInfo = _lib.ts3client_requestConnectionInfo
    ts3client_requestConnectionInfo.argtypes = [uint64, anyID, POINTER(c_char)]
    ts3client_requestConnectionInfo.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 167
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestClientSetWhisperList'):
        continue
    ts3client_requestClientSetWhisperList = _lib.ts3client_requestClientSetWhisperList
    ts3client_requestClientSetWhisperList.argtypes = [uint64, anyID, POINTER(uint64), POINTER(anyID), POINTER(c_char)]
    ts3client_requestClientSetWhisperList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 168
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelSubscribe'):
        continue
    ts3client_requestChannelSubscribe = _lib.ts3client_requestChannelSubscribe
    ts3client_requestChannelSubscribe.argtypes = [uint64, POINTER(uint64), POINTER(c_char)]
    ts3client_requestChannelSubscribe.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 169
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelSubscribeAll'):
        continue
    ts3client_requestChannelSubscribeAll = _lib.ts3client_requestChannelSubscribeAll
    ts3client_requestChannelSubscribeAll.argtypes = [uint64, POINTER(c_char)]
    ts3client_requestChannelSubscribeAll.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 170
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelUnsubscribe'):
        continue
    ts3client_requestChannelUnsubscribe = _lib.ts3client_requestChannelUnsubscribe
    ts3client_requestChannelUnsubscribe.argtypes = [uint64, POINTER(uint64), POINTER(c_char)]
    ts3client_requestChannelUnsubscribe.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 171
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelUnsubscribeAll'):
        continue
    ts3client_requestChannelUnsubscribeAll = _lib.ts3client_requestChannelUnsubscribeAll
    ts3client_requestChannelUnsubscribeAll.argtypes = [uint64, POINTER(c_char)]
    ts3client_requestChannelUnsubscribeAll.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 172
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestChannelDescription'):
        continue
    ts3client_requestChannelDescription = _lib.ts3client_requestChannelDescription
    ts3client_requestChannelDescription.argtypes = [uint64, uint64, POINTER(c_char)]
    ts3client_requestChannelDescription.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 173
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestMuteClients'):
        continue
    ts3client_requestMuteClients = _lib.ts3client_requestMuteClients
    ts3client_requestMuteClients.argtypes = [uint64, POINTER(anyID), POINTER(c_char)]
    ts3client_requestMuteClients.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 174
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestUnmuteClients'):
        continue
    ts3client_requestUnmuteClients = _lib.ts3client_requestUnmuteClients
    ts3client_requestUnmuteClients.argtypes = [uint64, POINTER(anyID), POINTER(c_char)]
    ts3client_requestUnmuteClients.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 175
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestClientIDs'):
        continue
    ts3client_requestClientIDs = _lib.ts3client_requestClientIDs
    ts3client_requestClientIDs.argtypes = [uint64, POINTER(c_char), POINTER(c_char)]
    ts3client_requestClientIDs.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 178
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestSlotsFromProvisioningServer'):
        continue
    ts3client_requestSlotsFromProvisioningServer = _lib.ts3client_requestSlotsFromProvisioningServer
    ts3client_requestSlotsFromProvisioningServer.argtypes = [POINTER(c_char), c_ushort, POINTER(c_char), c_ushort, POINTER(c_char), POINTER(c_char), POINTER(uint64)]
    ts3client_requestSlotsFromProvisioningServer.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 179
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_cancelRequestSlotsFromProvisioningServer'):
        continue
    ts3client_cancelRequestSlotsFromProvisioningServer = _lib.ts3client_cancelRequestSlotsFromProvisioningServer
    ts3client_cancelRequestSlotsFromProvisioningServer.argtypes = [uint64]
    ts3client_cancelRequestSlotsFromProvisioningServer.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 180
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_startConnectionWithProvisioningKey'):
        continue
    ts3client_startConnectionWithProvisioningKey = _lib.ts3client_startConnectionWithProvisioningKey
    ts3client_startConnectionWithProvisioningKey.argtypes = [uint64, POINTER(c_char), POINTER(c_char), POINTER(c_char)]
    ts3client_startConnectionWithProvisioningKey.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 185
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientID'):
        continue
    ts3client_getClientID = _lib.ts3client_getClientID
    ts3client_getClientID.argtypes = [uint64, POINTER(anyID)]
    ts3client_getClientID.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 188
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getConnectionStatus'):
        continue
    ts3client_getConnectionStatus = _lib.ts3client_getConnectionStatus
    ts3client_getConnectionStatus.argtypes = [uint64, POINTER(c_int)]
    ts3client_getConnectionStatus.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 189
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getConnectionVariableAsUInt64'):
        continue
    ts3client_getConnectionVariableAsUInt64 = _lib.ts3client_getConnectionVariableAsUInt64
    ts3client_getConnectionVariableAsUInt64.argtypes = [uint64, anyID, c_size_t, POINTER(uint64)]
    ts3client_getConnectionVariableAsUInt64.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 190
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getConnectionVariableAsDouble'):
        continue
    ts3client_getConnectionVariableAsDouble = _lib.ts3client_getConnectionVariableAsDouble
    ts3client_getConnectionVariableAsDouble.argtypes = [uint64, anyID, c_size_t, POINTER(c_double)]
    ts3client_getConnectionVariableAsDouble.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 191
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getConnectionVariableAsString'):
        continue
    ts3client_getConnectionVariableAsString = _lib.ts3client_getConnectionVariableAsString
    ts3client_getConnectionVariableAsString.argtypes = [uint64, anyID, c_size_t, POINTER(POINTER(c_char))]
    ts3client_getConnectionVariableAsString.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 192
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_cleanUpConnectionInfo'):
        continue
    ts3client_cleanUpConnectionInfo = _lib.ts3client_cleanUpConnectionInfo
    ts3client_cleanUpConnectionInfo.argtypes = [uint64, anyID]
    ts3client_cleanUpConnectionInfo.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 195
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestServerConnectionInfo'):
        continue
    ts3client_requestServerConnectionInfo = _lib.ts3client_requestServerConnectionInfo
    ts3client_requestServerConnectionInfo.argtypes = [uint64, POINTER(c_char)]
    ts3client_requestServerConnectionInfo.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 196
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getServerConnectionVariableAsUInt64'):
        continue
    ts3client_getServerConnectionVariableAsUInt64 = _lib.ts3client_getServerConnectionVariableAsUInt64
    ts3client_getServerConnectionVariableAsUInt64.argtypes = [uint64, c_size_t, POINTER(uint64)]
    ts3client_getServerConnectionVariableAsUInt64.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 197
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getServerConnectionVariableAsFloat'):
        continue
    ts3client_getServerConnectionVariableAsFloat = _lib.ts3client_getServerConnectionVariableAsFloat
    ts3client_getServerConnectionVariableAsFloat.argtypes = [uint64, c_size_t, POINTER(c_float)]
    ts3client_getServerConnectionVariableAsFloat.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 200
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientSelfVariableAsInt'):
        continue
    ts3client_getClientSelfVariableAsInt = _lib.ts3client_getClientSelfVariableAsInt
    ts3client_getClientSelfVariableAsInt.argtypes = [uint64, c_size_t, POINTER(c_int)]
    ts3client_getClientSelfVariableAsInt.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 201
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientSelfVariableAsString'):
        continue
    ts3client_getClientSelfVariableAsString = _lib.ts3client_getClientSelfVariableAsString
    ts3client_getClientSelfVariableAsString.argtypes = [uint64, c_size_t, POINTER(POINTER(c_char))]
    ts3client_getClientSelfVariableAsString.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 202
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setClientSelfVariableAsInt'):
        continue
    ts3client_setClientSelfVariableAsInt = _lib.ts3client_setClientSelfVariableAsInt
    ts3client_setClientSelfVariableAsInt.argtypes = [uint64, c_size_t, c_int]
    ts3client_setClientSelfVariableAsInt.restype = c_uint
    ts3client_setClientSelfVariableAsInt.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 203
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setClientSelfVariableAsString'):
        continue
    ts3client_setClientSelfVariableAsString = _lib.ts3client_setClientSelfVariableAsString
    ts3client_setClientSelfVariableAsString.argtypes = [uint64, c_size_t, POINTER(c_char)]
    ts3client_setClientSelfVariableAsString.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 204
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_flushClientSelfUpdates'):
        continue
    ts3client_flushClientSelfUpdates = _lib.ts3client_flushClientSelfUpdates
    ts3client_flushClientSelfUpdates.argtypes = [uint64, POINTER(c_char)]
    ts3client_flushClientSelfUpdates.restype = c_uint
    ts3client_flushClientSelfUpdates.errcheck = handle_error
    ts3client_flushClientSelfUpdates.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 206
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientVariableAsInt'):
        continue
    ts3client_getClientVariableAsInt = _lib.ts3client_getClientVariableAsInt
    ts3client_getClientVariableAsInt.argtypes = [uint64, anyID, c_size_t, POINTER(c_int)]
    ts3client_getClientVariableAsInt.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 207
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientVariableAsUInt64'):
        continue
    ts3client_getClientVariableAsUInt64 = _lib.ts3client_getClientVariableAsUInt64
    ts3client_getClientVariableAsUInt64.argtypes = [uint64, anyID, c_size_t, POINTER(uint64)]
    ts3client_getClientVariableAsUInt64.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 208
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientVariableAsString'):
        continue
    ts3client_getClientVariableAsString = _lib.ts3client_getClientVariableAsString
    ts3client_getClientVariableAsString.argtypes = [uint64, anyID, c_size_t, POINTER(POINTER(c_char))]
    ts3client_getClientVariableAsString.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 210
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getClientList'):
        continue
    ts3client_getClientList = _lib.ts3client_getClientList
    ts3client_getClientList.argtypes = [uint64, POINTER(POINTER(anyID))]
    ts3client_getClientList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 211
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelOfClient'):
        continue
    ts3client_getChannelOfClient = _lib.ts3client_getChannelOfClient
    ts3client_getChannelOfClient.argtypes = [uint64, anyID, POINTER(uint64)]
    ts3client_getChannelOfClient.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 214
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelVariableAsInt'):
        continue
    ts3client_getChannelVariableAsInt = _lib.ts3client_getChannelVariableAsInt
    ts3client_getChannelVariableAsInt.argtypes = [uint64, uint64, c_size_t, POINTER(c_int)]
    ts3client_getChannelVariableAsInt.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 215
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelVariableAsUInt64'):
        continue
    ts3client_getChannelVariableAsUInt64 = _lib.ts3client_getChannelVariableAsUInt64
    ts3client_getChannelVariableAsUInt64.argtypes = [uint64, uint64, c_size_t, POINTER(uint64)]
    ts3client_getChannelVariableAsUInt64.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 216
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelVariableAsString'):
        continue
    ts3client_getChannelVariableAsString = _lib.ts3client_getChannelVariableAsString
    ts3client_getChannelVariableAsString.argtypes = [uint64, uint64, c_size_t, POINTER(POINTER(c_char))]
    ts3client_getChannelVariableAsString.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 218
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelIDFromChannelNames'):
        continue
    ts3client_getChannelIDFromChannelNames = _lib.ts3client_getChannelIDFromChannelNames
    ts3client_getChannelIDFromChannelNames.argtypes = [uint64, POINTER(POINTER(c_char)), POINTER(uint64)]
    ts3client_getChannelIDFromChannelNames.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 219
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setChannelVariableAsInt'):
        continue
    ts3client_setChannelVariableAsInt = _lib.ts3client_setChannelVariableAsInt
    ts3client_setChannelVariableAsInt.argtypes = [uint64, uint64, c_int, c_int]
    ts3client_setChannelVariableAsInt.restype = c_uint
    ts3client_setChannelVariableAsInt.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 220
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setChannelVariableAsUInt64'):
        continue
    ts3client_setChannelVariableAsUInt64 = _lib.ts3client_setChannelVariableAsUInt64
    ts3client_setChannelVariableAsUInt64.argtypes = [uint64, uint64, c_size_t, uint64]
    ts3client_setChannelVariableAsUInt64.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 221
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_setChannelVariableAsString'):
        continue
    ts3client_setChannelVariableAsString = _lib.ts3client_setChannelVariableAsString
    ts3client_setChannelVariableAsString.argtypes = [uint64, uint64, c_int, c_char_p]
    ts3client_setChannelVariableAsString.restype = c_uint
    ts3client_setChannelVariableAsString.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 222
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_flushChannelUpdates'):
        continue
    ts3client_flushChannelUpdates = _lib.ts3client_flushChannelUpdates
    ts3client_flushChannelUpdates.argtypes = [uint64, uint64]
    ts3client_flushChannelUpdates.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 223
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_flushChannelCreation'):
        continue
    ts3client_flushChannelCreation = _lib.ts3client_flushChannelCreation
    ts3client_flushChannelCreation.argtypes = [uint64, uint64]
    ts3client_flushChannelCreation.restype = c_uint
    ts3client_flushChannelCreation.errcheck = handle_error
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 225
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelList'):
        continue
    ts3client_getChannelList = _lib.ts3client_getChannelList
    ts3client_getChannelList.argtypes = [uint64, POINTER(POINTER(uint64))]
    ts3client_getChannelList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 226
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getChannelClientList'):
        continue
    ts3client_getChannelClientList = _lib.ts3client_getChannelClientList
    ts3client_getChannelClientList.argtypes = [uint64, uint64, POINTER(POINTER(anyID))]
    ts3client_getChannelClientList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 227
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getParentChannelOfChannel'):
        continue
    ts3client_getParentChannelOfChannel = _lib.ts3client_getParentChannelOfChannel
    ts3client_getParentChannelOfChannel.argtypes = [uint64, uint64, POINTER(uint64)]
    ts3client_getParentChannelOfChannel.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 230
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getServerConnectionHandlerList'):
        continue
    ts3client_getServerConnectionHandlerList = _lib.ts3client_getServerConnectionHandlerList
    ts3client_getServerConnectionHandlerList.argtypes = [POINTER(POINTER(uint64))]
    ts3client_getServerConnectionHandlerList.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 231
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getServerVariableAsInt'):
        continue
    ts3client_getServerVariableAsInt = _lib.ts3client_getServerVariableAsInt
    ts3client_getServerVariableAsInt.argtypes = [uint64, c_size_t, POINTER(c_int)]
    ts3client_getServerVariableAsInt.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 232
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getServerVariableAsUInt64'):
        continue
    ts3client_getServerVariableAsUInt64 = _lib.ts3client_getServerVariableAsUInt64
    ts3client_getServerVariableAsUInt64.argtypes = [uint64, c_size_t, POINTER(uint64)]
    ts3client_getServerVariableAsUInt64.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 233
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_getServerVariableAsString'):
        continue
    ts3client_getServerVariableAsString = _lib.ts3client_getServerVariableAsString
    ts3client_getServerVariableAsString.argtypes = [uint64, c_size_t, POINTER(POINTER(c_char))]
    ts3client_getServerVariableAsString.restype = c_uint
    break

# /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 234
for _lib in _libs.values():
    if not hasattr(_lib, 'ts3client_requestServerVariables'):
        continue
    ts3client_requestServerVariables = _lib.ts3client_requestServerVariables
    ts3client_requestServerVariables.argtypes = [uint64]
    ts3client_requestServerVariables.restype = c_uint
    break

ClientUIFunctionsRare = struct_ClientUIFunctionsRare # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 23

ClientUIFunctions = struct_ClientUIFunctions # /home/mike/ts3/ts3_sdk_3.0.2/include/clientlib.h: 25

class TeamspeakClient(object):
    def __init__(self, log_directory = None, connect_status_callback = None, new_channel_callback = None, channel_created_callback = None, move_callback=None):
        self.connection_handler = None
        self.connect_status_callback = connect_status_callback
        self.new_channel_callback = new_channel_callback
        self.channel_created_callback = channel_created_callback
        self.move_callback = move_callback
        # Callbacks have to be defined here due to self argument
        # The idea here is to have python methods set on the instance for the relevant statuses (connected, disconnected etc.)
        def on_connect_status_change_callback(handler, status, error_number):
            if self.connect_status_callback is not None:
                self.connect_status_callback(status, error_number)
        
        def on_new_channel_callback(connection_handler, channel_id, channel_parent_id):
            if self.new_channel_callback is not None:
                self.new_channel_callback(channel_id, channel_parent_id)
        
        def on_new_channel_created_callback(connection_handler, channel_id, channel_parent_id, invoker_id, invoker_name, invoker_unique_identifier):
             if self.channel_created_callback is not None:
                self.channel_created_callback(channel_id, invoker_id) # All we care about right now
        
        def on_client_move_callback(connection_handler, client_id, old_channel_id, new_channel_id, visibility, move_message):
            if self.move_callback is not None:
                self.move_callback(client_id, old_channel_id, new_channel_id) #All we care about
        
        self.callbacks = ClientUIFunctions()
        self.callbacks.onConnectStatusChangeEvent = struct_ClientUIFunctions._fields_[0][1](on_connect_status_change_callback) # Modifying the null function to point to ours
        self.callbacks.onNewChannelEvent = struct_ClientUIFunctions._fields_[2][1](on_new_channel_callback)
        self.callbacks.onNewChannelCreatedEvent = struct_ClientUIFunctions._fields_[3][1](on_new_channel_created_callback)
        self.callbacks.onClientMoveEvent = struct_ClientUIFunctions._fields_[9][1](on_client_move_callback)
        should_log = 0
        if log_directory is not None:
            should_log = 1
            log_directory = log_directory.replace('\\', '/') + '/'
        # Initialize the client instance
        ts3client_initClientLib(self.callbacks, None, should_log, c_char_p(log_directory), c_char_p((os.getcwd().replace('\\', '/') + '/').encode()))
        # Spawn the identity
        identity = c_char_p()
        ts3client_createIdentity(byref(identity))
        self.identity = identity.value
    
    def connect(self, host, port, nickname):
        # Spawn a connection handler
        handler= uint64()
        ts3client_spawnNewServerConnectionHandler(0, byref(handler))
        self.connection_handler = handler.value
        # Connect!
        ts3client_startConnection(uint64(self.connection_handler), c_char_p(self.identity), c_char_p(host.encode()), c_uint(port), c_char_p(nickname.encode()), None, c_char_p(b''), c_char_p(b'secret'))
        # Get status
        status = c_int()
        ts3client_getConnectionStatus(uint64(self.connection_handler), byref(status))
    
    def disconnect(self):
        ts3client_stopConnection(uint64(self.connection_handler), c_char_p(b''))
    
    def destroy(self):
        ts3client_destroyClientLib()
    
    def activate_playback_device(self):
        # Use default device and mode for now, need to see if we want to allow customization
        ts3client_openPlaybackDevice(uint64(self.connection_handler), c_char_p(b''), c_char_p(b''))
    
    def activate_capture_device(self):
        ts3client_openCaptureDevice(uint64(self.connection_handler), c_char_p(b''), c_char_p(b''))
    
    def close_playback_device(self):
        ts3client_closePlaybackDevice(uint64(self.connection_handler))
        
    def close_capture_device(self):
        ts3client_closeCaptureDevice(uint64(self.connection_handler))
    
    def get_connection_status(self):
        if self.connection_handler is None:
            return STATUS_DISCONNECTED
        status = c_int()
        ts3client_getConnectionStatus(uint64(self.connection_handler), byref(status))
        return status.value
    
    def is_connected(self):
        # Returns true if connection is established, meaning we can join/create channels, etc.
        if self.get_connection_status() == STATUS_CONNECTION_ESTABLISHED:
            return True
        return False
    
    def get_client_id(self):
        client_id = anyID()
        ts3client_getClientID(uint64(self.connection_handler), byref(client_id))
        return client_id.value
    
    def mute(self):
        ts3client_setClientSelfVariableAsInt(uint64(self.connection_handler), CLIENT_INPUT_DEACTIVATED, c_int(INPUT_DEACTIVATED))
        ts3client_flushClientSelfUpdates(uint64(self.connection_handler), None)
    
    def unmute(self):
        ts3client_setClientSelfVariableAsInt(uint64(self.connection_handler), CLIENT_INPUT_DEACTIVATED, c_int(INPUT_ACTIVE))
        ts3client_flushClientSelfUpdates(uint64(self.connection_handler), None)
    
    def get_mute_status(self):
        status = c_int()
        ts3client_getClientSelfVariableAsInt(uint64(self.connection_handler), CLIENT_INPUT_DEACTIVATED, byref(status))
        return status.value
    
    def create_channel(self, name, topic, description='', password='', codec=1, codec_quality=7, max_clients=100, max_family_clients=100, order=0, permanent=False, semipermanent=False, default_channel=False, password_protected=False, codec_latency_factor=1):
        channel_id = uint64(0)

        # Encode to pytes for python3 error
        name = name.encode()
        topic = topic.encode()
        description = description.encode()
        password = password.encode()

        ts3client_setChannelVariableAsString(uint64(self.connection_handler), channel_id, CHANNEL_NAME, c_char_p(name))
        ts3client_setChannelVariableAsString(uint64(self.connection_handler), channel_id, CHANNEL_TOPIC, c_char_p(topic))
        ts3client_setChannelVariableAsString(uint64(self.connection_handler), channel_id, CHANNEL_DESCRIPTION, description)
        ts3client_setChannelVariableAsString(uint64(self.connection_handler), channel_id, CHANNEL_PASSWORD, password)
        ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_CODEC, c_int(codec))
        ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_CODEC_QUALITY, c_int(codec_quality))
        ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_MAXCLIENTS, c_int(max_clients))
        ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_MAXFAMILYCLIENTS, c_int(max_family_clients))
        #ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_ORDER, c_int(order))
        if permanent:
            ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_FLAG_PERMANENT, c_int(1))
        if semipermanent:
            ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_FLAG_SEMIPERMANENT, c_int(1))
        if default_channel:
            ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_FLAG_DEFAULT, c_int(1))
        if password_protected:
            ts3client_setChannelVariableAsInt(uint64(self.connection_handler), channel_id, CHANNEL_FLAG_PASSWORD, c_int(1))
        ts3client_flushChannelCreation(uint64(self.connection_handler), uint64(0))
    
    def join_channel(self, channel_id, password='', client_id=None):
        if client_id is None:
            client_id = self.get_client_id()
        ts3client_requestClientMove(uint64(self.connection_handler), anyID(client_id), uint64(channel_id), c_char_p(password.encode()), None)
    
    def mute_user(self, client_id):
        UserArray = anyID * 1
        ts3client_requestMuteClients(self.connection_handler, UserArray(anyID(client_id)), None)
    
    def unmute_user(self, client_id):
        UserArray = anyID * 1
        ts3client_requestUnmuteClients(self.connection_handler, UserArray(anyID(client_id)), None)
    
    def get_volume(self):
        volume = c_float()
        ts3client_getPlaybackConfigValueAsFloat(uint64(self.connection_handler), c_char_p(b'volume_modifier'), byref(volume))
        return volume.value
    
    def set_volume(self, volume):
        ts3client_setPlaybackConfigValue(uint64(self.connection_handler), c_char_p(b'volume_modifier'), c_char_p(str(volume).encode()))